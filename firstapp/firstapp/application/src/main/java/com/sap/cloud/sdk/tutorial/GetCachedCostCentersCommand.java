package com.sap.cloud.sdk.tutorial;

import java.util.Collections;
import java.util.List;
 
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
 
import com.sap.cloud.sdk.cloudplatform.cache.CacheKey;
import com.sap.cloud.sdk.odatav2.connectivity.ODataException;
import com.sap.cloud.sdk.odatav2.connectivity.ODataExceptionType;
import com.sap.cloud.sdk.odatav2.connectivity.ODataQueryBuilder;
import com.sap.cloud.sdk.s4hana.connectivity.CachingErpCommand;
import com.sap.cloud.sdk.s4hana.connectivity.ErpConfigContext;
 
import lombok.NonNull;
 
 class GetCachedCostCentersCommand extends CachingErpCommand<List<CostCenterDetails>>
{
    private static final Cache<CacheKey, List<CostCenterDetails>> cache =
        CacheBuilder.newBuilder().build();
 
    public GetCachedCostCentersCommand( @NonNull final ErpConfigContext configContext )
    {
        super(GetCachedCostCentersCommand.class, configContext);
    }

    @Override
    protected Cache<CacheKey, List<CostCenterDetails>> getCache()
    {
        return cache;
    }
 
    @Override
    protected List<CostCenterDetails> runCacheable() throws ODataException {
        try {
            final List<CostCenterDetails> costCenters = ODataQueryBuilder
                .withEntity("/sap/opu/odata/sap/FCO_PI_COST_CENTER", "CostCenterCollection")
                .build()
                .execute(getErpEndpoint())
                .asList(CostCenterDetails.class);
                     
            return costCenters;
        }
        catch( final Exception e) {
            throw new ODataException(ODataExceptionType.OTHER, "Failed to get CostCenters from OData command.", e);
        }
    }

    @Override
    protected List<CostCenterDetails> getFallback() {
        return Collections.emptyList();
    }
}