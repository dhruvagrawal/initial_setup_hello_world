package com.sap.cloud.sdk.tutorial;

import lombok.Data;
 
import com.sap.cloud.sdk.result.ElementName;
 
@Data
public class NorthWindData
{
    @ElementName( "CategoryID" )
    private String CategoryID;
 
    @ElementName( "CategoryName" )
    private String CategoryName;
 
    @ElementName( "Description" )
    private String Description;
 
}