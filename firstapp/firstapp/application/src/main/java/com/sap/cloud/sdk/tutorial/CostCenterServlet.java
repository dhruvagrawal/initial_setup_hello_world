package com.sap.cloud.sdk.tutorial;

import com.google.gson.Gson;
import org.slf4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import java.util.List;

@WebServlet("/costcenters")
public class CostCenterServlet extends HttpServlet {
	private static final String USER_AGENT = "Mozilla/5.0";
	private static final long serialVersionUID = 1L;
	//private static final Logger logger = CloudLoggerFactory.getLogger(CostCenterServlet.class);

	@Override
	protected void doGet( final HttpServletRequest request, final HttpServletResponse response )
			throws ServletException,
			IOException
	{
		try {
			/* final ErpEndpoint endpoint = new ErpEndpoint();
            final List<NorthWindData> costCenters = ODataQueryBuilder
                    .withEntity("/V4/Northwind/Northwind.svc/", "Categories")
                    .select("CategoryID", "CategoryName", "Description")
                    .build()
                    .execute(endpoint)
                    .asList(NorthWindData.class);*/
			URL obj = new URL("http://services.odata.org/V2/Northwind/Northwind.svc/Categories?$format=json");

			//URL obj = new URL(GET_URL);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setRequestMethod("GET");
			con.setRequestProperty("User-Agent", USER_AGENT);
			int responseCode = con.getResponseCode();
			System.out.println("GET Response Code :: " + responseCode);
			if (responseCode == HttpURLConnection.HTTP_OK) { // success
				BufferedReader in = new BufferedReader(new InputStreamReader(
						con.getInputStream()));
				String inputLine;
				StringBuffer responseData = new StringBuffer();

				while ((inputLine = in.readLine()) != null) {
					responseData.append(inputLine);
				}
				in.close();

				response.setContentType("application/json");
				
				response.getWriter().write(responseData.toString());
			} else {
				System.out.println("GET request not worked");
			}
			

		} catch(final Exception e) {
		//logger.error(e.getMessage(), e);
		response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		response.getWriter().write(e.getMessage());
	}
}
}