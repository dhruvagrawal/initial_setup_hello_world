package com.sap.cloud.sdk.tutorial;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;
 
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
 
import com.sap.cloud.sdk.cloudplatform.cache.CacheKey;
import com.sap.cloud.sdk.odatav2.connectivity.ODataException;
import com.sap.cloud.sdk.odatav2.connectivity.ODataExceptionType;
import com.sap.cloud.sdk.odatav2.connectivity.ODataProperty;
import com.sap.cloud.sdk.odatav2.connectivity.ODataQueryBuilder;
import com.sap.cloud.sdk.odatav2.connectivity.ODataType;
import com.sap.cloud.sdk.s4hana.connectivity.CachingErpCommand;
import com.sap.cloud.sdk.s4hana.connectivity.ErpConfigContext;
 
import lombok.NonNull;
 
 
class GetCostCentersByCompanyCodeCommand extends CachingErpCommand<List<CostCenterDetails>>
{
    @NonNull
    private final String companyCode;
    public GetCostCentersByCompanyCodeCommand(
            @NonNull final ErpConfigContext configContext,
            @NonNull final String companyCode )
    {
        super(GetCostCentersByCompanyCodeCommand.class, configContext);
        this.companyCode = companyCode;
    }
 
    private static final Cache<CacheKey, List<CostCenterDetails>> cache =
        CacheBuilder.newBuilder()
                .maximumSize(100)
                .expireAfterAccess(60, TimeUnit.SECONDS)
                .concurrencyLevel(10)
                .build();
 
    @Override
    protected Cache<CacheKey, List<CostCenterDetails>> getCache()
    {
        return cache;
    }
 
    @Override
    protected CacheKey getCommandCacheKey()
    {
        return super.getCommandCacheKey().append(companyCode);
    }
 
    @Override
    protected List<CostCenterDetails> runCacheable() throws ODataException {
        try {
            final List<CostCenterDetails> costCenters = ODataQueryBuilder
                .withEntity("/sap/opu/odata/sap/FCO_PI_COST_CENTER", "CostCenterCollection")
                .filter(ODataProperty.field("CompanyCode").eq(ODataType.of(companyCode)))
                .build()
                .execute(getErpEndpoint())
                .asList(CostCenterDetails.class);
             
            return costCenters;
        }
        catch( final Exception e) {
            throw new ODataException(ODataExceptionType.OTHER, "Failed to get CostCenters from OData command.", e);
        }
    }
     
    @Override
    protected List<CostCenterDetails> getFallback() {
        return Collections.emptyList();
    }
}